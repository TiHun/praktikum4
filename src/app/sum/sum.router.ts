import { Router } from "express";

export const router = Router(); 

router.get("/sum/:a/:b", (req, res) => {
  const a = parseInt(req.params.a, 10); // Parse 'a' parameter to a number
  const b = parseInt(req.params.b, 10); // Parse 'b' parameter to a number

  // Check if the parsed numbers are valid
  if (isNaN(a) && isNaN(b)) {
    return res
      .status(400)
      .send({ msg: "Invalid parameters. Both 'a' and 'b' should be numbers." });
  }
  const sum = a + b;

  return res.status(200).send({ msg: `Sum: ${sum}` });
});
